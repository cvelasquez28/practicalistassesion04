package com.cfperuweb.practicalistassesion04.interfaces;

import android.view.View;

import com.cfperuweb.practicalistassesion04.model.Contacto;

public interface IOnClickContacto {

    void onItemClick(View v, Contacto contacto, int posicion);
}
