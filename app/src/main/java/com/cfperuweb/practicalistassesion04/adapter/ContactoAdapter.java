package com.cfperuweb.practicalistassesion04.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.cfperuweb.practicalistassesion04.R;
import com.cfperuweb.practicalistassesion04.interfaces.IOnClickContacto;
import com.cfperuweb.practicalistassesion04.model.Contacto;

import java.util.List;
import java.util.Random;

public class ContactoAdapter extends RecyclerView.Adapter<ContactoAdapter.ContactoAdapterViewHolder> {

    Context context;
    List<Contacto> items;
    IOnClickContacto iOnClickContacto;

    public void setiOnClickContacto(IOnClickContacto iOnClickContacto) {
        this.iOnClickContacto = iOnClickContacto;
    }

    public ContactoAdapter(Context context, List<Contacto> items) {
        this.context = context;
        this.items = items;
    }

    @NonNull
    @Override
    public ContactoAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_contacto, parent, false);
        return new ContactoAdapterViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ContactoAdapterViewHolder holder, final int position) {
        final Contacto contacto = items.get(position);

        holder.tvNombres.setText(contacto.getNombres());
        holder.tvPerfil.setText(contacto.getPerfil());
        holder.tvCorreo.setText(contacto.getCorreo());
        holder.tvInicial.setText(String.valueOf(contacto.getNombres().charAt(0)));

        int[] androidColors = context.getResources().getIntArray(R.array.colores_array);
        int randomAndroidColor = androidColors[new Random().nextInt(androidColors.length)];
        holder.llInicial.setBackgroundColor(randomAndroidColor);


        holder.ivLlamar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               iOnClickContacto.onItemClick(v, contacto, position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public class ContactoAdapterViewHolder extends RecyclerView.ViewHolder {

        TextView tvNombres, tvPerfil, tvCorreo, tvInicial;
        ImageView ivLlamar;
        LinearLayout llInicial;

        public ContactoAdapterViewHolder(@NonNull View itemView) {
            super(itemView);
            tvNombres = itemView.findViewById(R.id.tvNombres);
            tvPerfil = itemView.findViewById(R.id.tvPerfil);
            tvCorreo = itemView.findViewById(R.id.tvCorreo);
            tvInicial = itemView.findViewById(R.id.tvInicial);
            ivLlamar = itemView.findViewById(R.id.ivLlamar);
            llInicial = itemView.findViewById(R.id.llInicial);
        }
    }
}
