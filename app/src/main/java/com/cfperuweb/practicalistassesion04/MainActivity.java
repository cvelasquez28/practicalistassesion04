package com.cfperuweb.practicalistassesion04;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.cfperuweb.practicalistassesion04.adapter.ContactoAdapter;
import com.cfperuweb.practicalistassesion04.interfaces.IOnClickContacto;
import com.cfperuweb.practicalistassesion04.model.Contacto;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";
    List<Contacto> items = new ArrayList<>();
    RecyclerView rvContactos;
    ContactoAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        inicializarUI();
        cargarDataContacto(this);
        configurarAdapter();
    }

    private void inicializarUI() {

        rvContactos = findViewById(R.id.rvContactos);
    }

    public  void cargarDataContacto(Context ctx) {


        String nombre_arr[] = ctx.getResources().getStringArray(R.array.contacto_nombres);
        String perfil_arr[] = ctx.getResources().getStringArray(R.array.contacto_perfil);
        String correo_arr[] = ctx.getResources().getStringArray(R.array.contacto_correo);
        String telefono_arr[] = ctx.getResources().getStringArray(R.array.contacto_telefono);

        for (int i = 0; i < nombre_arr.length; i++) {
            Contacto contacto  = new Contacto();
            contacto.setNombres(nombre_arr[i]);
            contacto.setPerfil(perfil_arr[i]);
            contacto.setCorreo(correo_arr[i]);
            contacto.setTelefono(telefono_arr[i]);
            items.add(contacto);
        }
    }

    private void configurarAdapter() {
        adapter = new ContactoAdapter(this,items);
        rvContactos.setLayoutManager(new LinearLayoutManager(this));
        rvContactos.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        rvContactos.setAdapter(adapter);
        adapter.setiOnClickContacto(new IOnClickContacto() {
            @Override
            public void onItemClick(View v, Contacto contacto, int posicion) {
                Log.e(TAG, "onItemClick: "+v.getId() );
                Intent i = new Intent(Intent.ACTION_DIAL);
                i.setData(Uri.parse("tel:"+contacto.getTelefono()));
                startActivity(i);
            }
        });
    }

}